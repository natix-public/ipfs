package io.natix.ipfs.web;

import io.ipfs.api.IPFSTools;
import io.ipfs.multihash.Multihash;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.Map;

@RestController
public class WebRestController implements IWebRestController {



    @Override
    public ResponseEntity<String> put(String name, byte[] data) throws IOException {
        String res = IPFSTools.put(name, data);
        return new ResponseEntity<>(res, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<byte[]> pull(String hash) throws IOException {

        byte[] res = IPFSTools.pull(hash);
        if (res == null)
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(res, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Map> statOfRepo() throws IOException {
        return new ResponseEntity<>(IPFSTools.statOfRepo(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Map> statOfFile(String hash) throws IOException {

        Map map = IPFSTools.statOfFile(hash);
        if (map == null)
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(map, HttpStatus.OK);

    }

    @Override
    public ResponseEntity addPeer(String ip, String hash) throws IOException {
        IPFSTools.addPeer(ip, hash);
        return new ResponseEntity(HttpStatus.OK);
    }

    @Override
    public ResponseEntity delPeer(String ip, String hash) throws IOException {
        IPFSTools.delPeer(ip, hash);
        return new ResponseEntity(HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Map> info() throws IOException {
        return new ResponseEntity<>(IPFSTools.info(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Map> info(String hash) throws IOException {
        return new ResponseEntity<>(IPFSTools.info(hash), HttpStatus.OK);
    }


}
