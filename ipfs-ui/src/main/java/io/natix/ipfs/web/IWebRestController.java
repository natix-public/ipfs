package io.natix.ipfs.web;


import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;
import java.util.Map;


public interface IWebRestController {

    @RequestMapping(value = "/", method = RequestMethod.POST)
    ResponseEntity<String> put(@RequestParam String name, @RequestParam byte[] data) throws IOException;

    @RequestMapping(value = "/{hash}", method = RequestMethod.GET)
    ResponseEntity<byte[]> pull(@PathVariable String hash) throws IOException;

    @RequestMapping(value = "/stat", method = RequestMethod.GET)
    ResponseEntity<Map> statOfRepo() throws IOException;

    @RequestMapping(value = "/stat/{hash}", method = RequestMethod.GET)
    ResponseEntity<Map> statOfFile(@PathVariable String hash) throws IOException;

    @RequestMapping(value = "/swarm/{ip}/{hash}", method = RequestMethod.POST)
    ResponseEntity addPeer(@PathVariable String ip, @PathVariable String hash) throws IOException;

    @RequestMapping(value = "/swarm/{ip}/{hash}", method = RequestMethod.DELETE)
    ResponseEntity delPeer(@PathVariable String ip, @PathVariable String hash) throws IOException;

    @RequestMapping(value = "/info", method = RequestMethod.GET)
    ResponseEntity<Map> info() throws IOException;

    @RequestMapping(value = "/info/{hash}", method = RequestMethod.GET)
    ResponseEntity<Map> info(@PathVariable String hash) throws IOException;


}
