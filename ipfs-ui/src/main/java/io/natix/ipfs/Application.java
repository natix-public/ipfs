package io.natix.ipfs;


import io.ipfs.api.IPFSTools;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.*;

@SpringBootApplication
public class Application {
    public static void main(String[] args) throws Exception {
        SpringApplication.run(Application.class, args);
        IPFSTools.subscribe("test", o -> {
            try {
                System.out.println(IPFSTools.put("test", o));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }, e -> {
            e.printStackTrace();
        });
        Thread.sleep(1000);
        IPFSTools.publish("test", new FileInputStream(new File("/Users/alirezaghias/Downloads/LICENSE.md")).readAllBytes());
        System.out.println("DOne");
    }


}
