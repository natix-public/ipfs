##IPFS Wrapper JAVA

This library is a wrapper on ipfs http api which can be used in java language.

By default this library assumes ipfs api server is listening on port 5001, you can change the variable in 
IPFSTools class.

####List of methods

1. pull (https://docs.ipfs.io/reference/http/api/#api-v0-cat)
2. statOfRepo (https://docs.ipfs.io/reference/http/api/#api-v0-stats-repo)
3. statOfFile (https://docs.ipfs.io/reference/http/api/#api-v0-object-stat)
4. addPeer (https://docs.ipfs.io/reference/http/api/#api-v0-swarm-connect)
5. delPeer (https://docs.ipfs.io/reference/http/api/#api-v0-swarm-disconnect)
6. swarms (https://docs.ipfs.io/reference/http/api/#api-v0-swarm-peers)
7. info (https://docs.ipfs.io/reference/http/api/#api-v0-id)
8. subscribe (https://docs.ipfs.io/reference/http/api/#api-v0-pubsub-sub)
9. public (https://docs.ipfs.io/reference/http/api/#api-v0-pubsub-pub)
10. remove (https://docs.ipfs.io/reference/http/api/#api-v0-pin-rm)

