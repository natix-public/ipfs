package io.ipfs.api;

import io.ipfs.multiaddr.MultiAddress;
import io.ipfs.multihash.Multihash;
import org.apache.commons.codec.binary.Base64;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

public class IPFSTools {
    private static IPFS ipfs = new IPFS("/ip4/127.0.0.1/tcp/5001");
    public static String put(String name, byte[] data) throws IOException {
        MerkleNode node = ipfs.add(new NamedStreamable.ByteArrayWrapper(name, data)).get(0);
        return node.hash.toBase58();
    }


    public static byte[] pull(String hash) throws IOException {
        try {
            ipfs.object.stat(Multihash.fromBase58(hash));
        } catch (Exception ex) {
            return null;
        }
        return ipfs.cat(Multihash.fromBase58(hash));
    }


    public static Map statOfRepo() throws IOException {
        return ipfs.stats.repo();
    }


    public static Map statOfFile(String hash) {
        try {
            return ipfs.object.stat(Multihash.fromBase58(hash));
        } catch (Exception ex) {
            return null;
        }
    }


    public static void addPeer(String ip, String hash) throws IOException {
        ipfs.swarm.connect(new MultiAddress("/ip4/" + ip + "/tcp/4001/ipfs/" + hash));
    }


    public static void delPeer(String ip, String hash) throws IOException {
        ipfs.swarm.disconnect(new MultiAddress("/ip4/" + ip + "/tcp/4001/ipfs/" + hash));
    }

    public static Map<Multihash, List<MultiAddress>> swarms() throws IOException {
        return ipfs.swarm.addrs();
    }


    public static Map info() throws IOException {
        return ipfs.id();
    }


    public static Map info(String hash) throws IOException {
        return ipfs.id(Multihash.fromBase58(hash));
    }

    public static void subscribe(String topic, Consumer<byte[]> consumer, Consumer<IOException> exceptionConsumer) throws IOException {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    ipfs.pubsub.sub(topic, stringObjectMap -> {
                        if (stringObjectMap.get("data") != null && stringObjectMap.get("data") instanceof String) {
                            byte[] res = Base64.decodeBase64((String)stringObjectMap.get("data"));
                            if (Base64.isBase64(res))
                                consumer.accept(Base64.decodeBase64(res));
                            else consumer.accept(res);
                        }
                    }, exceptionConsumer);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();

    }

    public static void publish(String topic, byte[] data) throws Exception {
        ipfs.pubsub.pub(topic, Base64.encodeBase64URLSafeString(data));
    }

    public static void remove(String hash) {
        if (statOfFile(hash) != null) {
            try {
                ipfs.pin.rm(Multihash.fromBase58(hash));
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                ipfs.repo.gc();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
